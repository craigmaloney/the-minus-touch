# The Minus Touch

This is a solo game entered into the One Hit Point game jam by Sage Latorra and Adam Blinkinsop

This game is intended for one person, but can be played with multiple people. 

By Craig Maloney

Released for the One Hit Point Game Jam

(Licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/))
