# The Minus Touch
By Craig Maloney

Released for the One Hit Point Game Jam

(Licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/))


## Introduction

You've been given a gift, the gift to make anything you touch disappear.

This game is intended for one person, but can be played with multiple people. 

For the next 15 minutes you will have the power to make anything disappear merely by touching it. 

## What you'll need

You'll need a timer that is capable of being set for 15 minutes. A wearable device on your wrist that has a timer is preferred, but any timer will suffice. 

This will work in any location, but please see the "play responsibly" section below.

## Play responsibly

Please remember to "disappear" things responsibly. Remember that this is a game, and that some folks might still have attachments to whatever you're disappearing. Also remember to get the consent of others that are in your space who might not be playing. 

## How to play

Begin by heading to the location in which you will be playing. This can be somewhere as simple as your house, or a park, or even a shopping mall. Just be mindful of who is participating and who isn't participating.

Set the timer for 15 minutes (see "variations" below for other time settings). Start the timer.

For the duration of the timer anything you "touch" with your hands immediately no longer exists. It is as though it disappeared with your touch.

Disappearing means you play as though the object no longer exists. This may be something as simple as pretending that a television you touched no longer exists and is now invisible to you. It might also be more elaborate. Let your mind explore the possibilities.

### Surfaces

If what you are touching is suspended in the air by your touching it you may place the object on a "surface" (either on a table, counter, or ground) without the surface itself disappearing. Surfaces such as floors, the ground outside, or shelves are somehow immune to your touch.

*Example: Angie starts the game with a cup of coffee in her hand. Once the timer starts the cup of coffee "disappears". She is permitted to place the cup of coffee on the counter (a surface) without the counter itself disappearing. This is the preferred way to make a cup of coffee disappear for the duration of the game.*

Breakable objects are required to go on a surface in such a way that they do not break for the duration of the game. Once the game is over your breakable object may follow whatever rules they normally follow.

### Sentient beings

If you touch a living being (a person, a pet, or something else with sentience) they have "disappeared" for the duration of the game. They might not understand this at first, so if they do not agree to your declaration of being "disappeared" then they reappear and are no longer eligible to be disappeared for the duration of the game. 

(Note: consent is key in this game, so please make sure that you have the consent of all of those who you touch. See "play responsibly" above).

*Example: Alice has a dog, Scrufffles, who wants to be petted while Alice is playing the game. Alice reaches down and pets Scruffles. In game terms, Scruffles has "disappeared". Alice keeps playing as though Scruffles has disappeared. Scruffles doesn't understand and starts to bark his disapproval. Because Scruffles doesn't agree to being "disappeared" Scruffles now reappears and is no longer eligible to disappear for the duration of the game. Scruffles is content with this outcome and the game continues.*

## Ending the game

Once the timer expires then everything reappears and the game is ended. Make sure that everything is returned to where it was before you touched it.

## Variations

### Short-term, Long term
15 minutes is just a guideline, but you can play for as long as you might like. Some folks might be satisfied with 10 minutes, while others might find enjoyment with over an hour. Whatever works for you and those around you.

### Multiple Minus Touch
Multiple people may play this game. Players are subject to the rules above about "sentient beings", and may object to being disappeared. Players who object can return as though they were not touched. They may still participate for the duration of the game. 

### Absolute Value
If, after the end of the game you realize that what you touched no longer holds any value to you, you may discard it permanently. Note that the consent rules above still apply.

### Cleaning the room
This activity could also be part of a cleaning ritual where things can "disappear". This could be great for yourself or a child. Granted you might want to be kind and have a "no, I didn't really want to 'disappear' that", but feel free to play with the idea.

## Designer notes

This is based off of the Greek Myth about King Midas and The Midas Touch. In the myth King Midas is given the "gift" of turning anything he touches into gold, with predictably disastrous results. The game is also based on the idea of making things disappear in our lives that are troublesome or otherwise no longer bring us joy. I hope you enjoy this little thought experiment.
